package com.omkingo.codechallenge.model;

import java.util.List;

public class ResponseObject {


    /**
     * items : [{"tags":["java","arrays","string","multidimensional-array"],"owner":{"reputation":15,"user_id":8491933,"user_type":"registered","profile_image":"https://lh5.googleusercontent.com/-ZwPFA5Tmfi8/AAAAAAAAAAI/AAAAAAAAAB0/o2bwoft69X0/photo.jpg?sz=128","display_name":"Andy","link":"https://stackoverflow.com/users/8491933/andy"},"is_answered":false,"view_count":40,"closed_date":1509987598,"answer_count":1,"score":-1,"last_activity_date":1509987727,"creation_date":1509983632,"last_edit_date":1509987568,"question_id":47140855,"link":"https://stackoverflow.com/questions/47140855/convert-string-to-multidimensional-array","closed_reason":"duplicate","title":"Convert String to multidimensional Array"},{"tags":["java","spring","events","spring-boot"],"owner":{"reputation":75,"user_id":7544861,"user_type":"registered","accept_rate":60,"profile_image":"https://www.gravatar.com/avatar/0886dc781a88dd8d8911b732182d7443?s=128&d=identicon&r=PG&f=1","display_name":"Victor","link":"https://stackoverflow.com/users/7544861/victor"},"is_answered":false,"view_count":121,"answer_count":2,"score":0,"last_activity_date":1509987720,"creation_date":1491918165,"question_id":43347739,"link":"https://stackoverflow.com/questions/43347739/using-eventuate-framework","title":"Using Eventuate framework"},{"tags":["java"],"owner":{"reputation":1,"user_id":7717395,"user_type":"registered","profile_image":"https://www.gravatar.com/avatar/96f5c923a79f3df6c655431c0761c23e?s=128&d=identicon&r=PG&f=1","display_name":"Sashank Singh","link":"https://stackoverflow.com/users/7717395/sashank-singh"},"is_answered":false,"view_count":30,"closed_date":1509987740,"answer_count":1,"score":-3,"last_activity_date":1509987717,"creation_date":1509987136,"question_id":47141935,"link":"https://stackoverflow.com/questions/47141935/how-to-call-static-block-of-another-class-to-class-containing-main-method-in-jav","closed_reason":"unclear what you&#39;re asking","title":"How to call static block of another class to class containing main method in Java?"},{"tags":["java","jsoup"],"owner":{"reputation":1,"user_id":6569348,"user_type":"registered","profile_image":"https://graph.facebook.com/1222984217726213/picture?type=large","display_name":"Phalgun Krishna","link":"https://stackoverflow.com/users/6569348/phalgun-krishna"},"is_answered":false,"view_count":5,"closed_date":1509987770,"answer_count":0,"score":-1,"last_activity_date":1509987714,"creation_date":1509987714,"question_id":47142095,"link":"https://stackoverflow.com/questions/47142095/java-code-to-download-file-anchor-tag-as-filename","closed_reason":"duplicate","title":"java code to download file, anchor tag as filename"},{"tags":["java","ssh","expect","router","reboot"],"owner":{"reputation":1,"user_id":8895394,"user_type":"registered","profile_image":"https://www.gravatar.com/avatar/bbea6abf2e6d90f12c0001869628073f?s=128&d=identicon&r=PG&f=1","display_name":"Alberto","link":"https://stackoverflow.com/users/8895394/alberto"},"is_answered":false,"view_count":2,"answer_count":0,"score":0,"last_activity_date":1509987706,"creation_date":1509987706,"question_id":47142091,"link":"https://stackoverflow.com/questions/47142091/java-programme-to-reboot-router-with-jsch","title":"Java programme to reboot router with JSch"}]
     * has_more : true
     * quota_max : 10000
     * quota_remaining : 9962
     */

    private boolean has_more;
    private int quota_max;
    private int quota_remaining;
    private List<Questions> items;

    public boolean isHas_more() {
        return has_more;
    }

    public void setHas_more(boolean has_more) {
        this.has_more = has_more;
    }

    public int getQuota_max() {
        return quota_max;
    }

    public void setQuota_max(int quota_max) {
        this.quota_max = quota_max;
    }

    public int getQuota_remaining() {
        return quota_remaining;
    }

    public void setQuota_remaining(int quota_remaining) {
        this.quota_remaining = quota_remaining;
    }

    public List<Questions> getItems() {
        return items;
    }

    public void setItems(List<Questions> items) {
        this.items = items;
    }

    public static class Questions {
        /**
         * tags : ["java","arrays","string","multidimensional-array"]
         * owner : {"reputation":15,"user_id":8491933,"user_type":"registered","profile_image":"https://lh5.googleusercontent.com/-ZwPFA5Tmfi8/AAAAAAAAAAI/AAAAAAAAAB0/o2bwoft69X0/photo.jpg?sz=128","display_name":"Andy","link":"https://stackoverflow.com/users/8491933/andy"}
         * is_answered : false
         * view_count : 40
         * closed_date : 1509987598
         * answer_count : 1
         * score : -1
         * last_activity_date : 1509987727
         * creation_date : 1509983632
         * last_edit_date : 1509987568
         * question_id : 47140855
         * link : https://stackoverflow.com/questions/47140855/convert-string-to-multidimensional-array
         * closed_reason : duplicate
         * title : Convert String to multidimensional Array
         */

        private Owner owner;
        private boolean is_answered;
        private int view_count;
        private int closed_date;
        private int answer_count;
        private int score;
        private int last_activity_date;
        private int creation_date;
        private int last_edit_date;
        private int question_id;
        private String link;
        private String closed_reason;
        private String title;
        private List<String> tags;

        public Owner getOwner() {
            return owner;
        }

        public void setOwner(Owner owner) {
            this.owner = owner;
        }

        public boolean isIs_answered() {
            return is_answered;
        }

        public void setIs_answered(boolean is_answered) {
            this.is_answered = is_answered;
        }

        public int getView_count() {
            return view_count;
        }

        public void setView_count(int view_count) {
            this.view_count = view_count;
        }

        public int getClosed_date() {
            return closed_date;
        }

        public void setClosed_date(int closed_date) {
            this.closed_date = closed_date;
        }

        public int getAnswer_count() {
            return answer_count;
        }

        public void setAnswer_count(int answer_count) {
            this.answer_count = answer_count;
        }

        public int getScore() {
            return score;
        }

        public void setScore(int score) {
            this.score = score;
        }

        public int getLast_activity_date() {
            return last_activity_date;
        }

        public void setLast_activity_date(int last_activity_date) {
            this.last_activity_date = last_activity_date;
        }

        public int getCreation_date() {
            return creation_date;
        }

        public void setCreation_date(int creation_date) {
            this.creation_date = creation_date;
        }

        public int getLast_edit_date() {
            return last_edit_date;
        }

        public void setLast_edit_date(int last_edit_date) {
            this.last_edit_date = last_edit_date;
        }

        public int getQuestion_id() {
            return question_id;
        }

        public void setQuestion_id(int question_id) {
            this.question_id = question_id;
        }

        public String getLink() {
            return link;
        }

        public void setLink(String link) {
            this.link = link;
        }

        public String getClosed_reason() {
            return closed_reason;
        }

        public void setClosed_reason(String closed_reason) {
            this.closed_reason = closed_reason;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public List<String> getTags() {
            return tags;
        }

        public void setTags(List<String> tags) {
            this.tags = tags;
        }

        public static class Owner {
            /**
             * reputation : 15
             * user_id : 8491933
             * user_type : registered
             * profile_image : https://lh5.googleusercontent.com/-ZwPFA5Tmfi8/AAAAAAAAAAI/AAAAAAAAAB0/o2bwoft69X0/photo.jpg?sz=128
             * display_name : Andy
             * link : https://stackoverflow.com/users/8491933/andy
             */

            private int reputation;
            private int user_id;
            private String user_type;
            private String profile_image;
            private String display_name;
            private String link;

            public int getReputation() {
                return reputation;
            }

            public void setReputation(int reputation) {
                this.reputation = reputation;
            }

            public int getUser_id() {
                return user_id;
            }

            public void setUser_id(int user_id) {
                this.user_id = user_id;
            }

            public String getUser_type() {
                return user_type;
            }

            public void setUser_type(String user_type) {
                this.user_type = user_type;
            }

            public String getProfile_image() {
                return profile_image;
            }

            public void setProfile_image(String profile_image) {
                this.profile_image = profile_image;
            }

            public String getDisplay_name() {
                return display_name;
            }

            public void setDisplay_name(String display_name) {
                this.display_name = display_name;
            }

            public String getLink() {
                return link;
            }

            public void setLink(String link) {
                this.link = link;
            }
        }
    }
}
