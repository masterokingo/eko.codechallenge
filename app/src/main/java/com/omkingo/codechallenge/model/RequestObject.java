package com.omkingo.codechallenge.model;

public class RequestObject {

    private String order = "desc";
    private String sort = "activity";
    private String tagged = "java";
    private String site = "stackoverflow";
    private String intitle = "";
    private int page = 1;

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getTagged() {
        return tagged;
    }

    public void setTagged(String tagged) {
        this.tagged = tagged;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getIntitle() {
        return intitle;
    }

    public void setIntitle(String intitle) {
        this.intitle = intitle;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }
}
