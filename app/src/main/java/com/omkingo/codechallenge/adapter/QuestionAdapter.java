package com.omkingo.codechallenge.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.omkingo.codechallenge.R;
import com.omkingo.codechallenge.model.ResponseObject;
import java.util.List;

/**
 * Created by omkingo on 9/11/17.
 */

public class QuestionAdapter extends ArrayAdapter<ResponseObject.Questions> {

    private Context mContext;

    public QuestionAdapter(Context context, List<ResponseObject.Questions> types){
        super(context, R.layout.fragment_item, types);
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        // Get the data item for this position
        final ResponseObject.Questions itemModel = getItem(position);

        final ViewHolder viewHolder; // view lookup cache stored in tag

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.fragment_item, parent, false);
            viewHolder.itemContainer = convertView.findViewById(R.id.item_container);
            viewHolder.ivUserImage = convertView.findViewById(R.id.iv_user_image);
            viewHolder.tvUserName = convertView.findViewById(R.id.tv_user_name);
            viewHolder.tvQuestionTitle = convertView.findViewById(R.id.tv_question_title);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if (itemModel != null) {
            Glide.with(mContext).load(itemModel.getOwner().getProfile_image())
                    .apply(RequestOptions.circleCropTransform())
                    .into(viewHolder.ivUserImage);

            viewHolder.tvUserName.setText(itemModel.getOwner().getDisplay_name());
            viewHolder.tvQuestionTitle.setText(itemModel.getTitle());

            viewHolder.itemContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //Open browser with given link
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(itemModel.getLink()));
                    mContext.startActivity(browserIntent);
                }
            });
        }

        return convertView;
    }

    private static class ViewHolder {
        LinearLayout itemContainer;
        ImageView ivUserImage;
        TextView tvUserName;
        TextView tvQuestionTitle;
    }
}
