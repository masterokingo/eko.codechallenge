package com.omkingo.codechallenge.activity;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.omkingo.codechallenge.R;
import com.omkingo.codechallenge.adapter.MainPagerAdapter;
import com.omkingo.codechallenge.fragment.ResultFragment;

import java.util.ArrayList;

public class MainActivity extends BaseActivity {

    private ViewPager view_pager;
    private TabLayout tabLayout;


    @Override
    protected int getLayoutView() {
        return R.layout.activity_main;
    }

    @Override
    protected void bindView() {
        view_pager = findViewById(R.id.view_pager);
        tabLayout = findViewById(R.id.tab_layout);
    }

    @Override
    protected void setupView() {
    }

    @Override
    protected void initActivity() {
        tabLayout.addTab(tabLayout.newTab().setText(R.string.text_browse));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.text_search));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        ArrayList<Fragment> fragments = new ArrayList<>();

        fragments.add(ResultFragment.newInstance(ResultFragment.ACTION_BROWSE));
        fragments.add(ResultFragment.newInstance(ResultFragment.ACTION_SEARCH));

        MainPagerAdapter mPagerAdapter = new MainPagerAdapter(getSupportFragmentManager(), fragments);
        view_pager.setAdapter(mPagerAdapter);
        view_pager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                view_pager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    public void onClick(View view) {
    }
}
