package com.omkingo.codechallenge.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

abstract class BaseActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutView());
        bindView();
        setupInstance();
        setupView();
        initActivity();
    }

    protected abstract int getLayoutView();

    protected abstract void bindView();

    protected void setupInstance(){}

    protected abstract void setupView();

    protected void initActivity(){}

}
