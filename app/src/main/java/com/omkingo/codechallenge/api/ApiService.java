package com.omkingo.codechallenge.api;

import com.omkingo.codechallenge.converter.ToStringConverterFactory;
import com.omkingo.codechallenge.model.RequestObject;
import com.omkingo.codechallenge.model.ResponseObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.omkingo.codechallenge.fragment.ResultFragment.ACTION_BROWSE;
import static com.omkingo.codechallenge.fragment.ResultFragment.ACTION_SEARCH;

public class ApiService {

    private int mFragmentType = 0;

    public ApiService (int type){
        mFragmentType = type;
    }

    private static final String BASE_URL = "https://api.stackexchange.com/2.2/";

    private OnRequestObjectListener mListener;

    private Callback<ResponseObject> mCallback = new Callback<ResponseObject>() {
        @Override
        public void onResponse(Call<ResponseObject> call, Response<ResponseObject> response) {
            if (response.isSuccessful() && response.code() == 200 && response.body() != null) {
                if(mListener != null) mListener.onResponse(call, response.body());
            }else {
                System.out.println("onResponse failure " + response.message() + " " + response.raw().request().url().toString());
                if(mListener != null) mListener.onFailure(response.code(), response.message(), response.raw().request().url().toString());
            }
        }

        @Override
        public void onFailure(Call<ResponseObject> call, Throwable t) {
            t.printStackTrace();
            if(mListener != null) mListener.onFailure(500, t.getMessage(), call.request().url().toString());
        }
    };

    private ApiEndpointInterface getApiEndPointInterface() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(new ToStringConverterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit.create(ApiEndpointInterface.class);
    }

    public void callApi(final RequestObject requestObject, final OnRequestObjectListener listener) {
        this.mListener = listener;


        Call<ResponseObject> onApiCalls = getApiEndPointInterface().browseQuestion(requestObject.getTagged());
        switch (mFragmentType){
            case ACTION_BROWSE:
                onApiCalls = getApiEndPointInterface().browseQuestion();
                onApiCalls.enqueue(mCallback);
                break;
            case ACTION_SEARCH:
                onApiCalls = getApiEndPointInterface().searchQuestion(requestObject.getIntitle());
                onApiCalls.enqueue(mCallback);
                break;
        }


    }



}
