package com.omkingo.codechallenge.api;


import com.omkingo.codechallenge.model.RequestObject;
import com.omkingo.codechallenge.model.ResponseObject;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiEndpointInterface {

    @GET("questions?order=desc&sort=activity&site=stackoverflow")
    Call<ResponseObject> browseQuestion();

    @GET("questions?order=desc&sort=activity&site=stackoverflow")
    Call<ResponseObject> browseQuestion(@Query("tagged") String tagged);

    @GET("search?order=desc&sort=activity&site=stackoverflow")
    Call<ResponseObject> searchQuestion(@Query("intitle") String intitle);
}