package com.omkingo.codechallenge.api;

import com.omkingo.codechallenge.model.ResponseObject;

import retrofit2.Call;

public interface OnRequestObjectListener<T> {

    void onResponse(Call<T> call, ResponseObject data);
    void onFailure(int code, String message, String url);

}