package com.omkingo.codechallenge.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

public abstract class BaseFragment extends Fragment{

    protected LayoutInflater mInflater;
    private View mView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(getInflaterLayout(), container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mInflater = getActivity().getLayoutInflater();
        mView = view;
        mView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });

        if (mView instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) mView).getChildCount(); i++) {
                View innerView = ((ViewGroup) mView).getChildAt(i);
                if(!(innerView instanceof EditText)) {
                    innerView.setOnTouchListener(new View.OnTouchListener() {
                        public boolean onTouch(View v, MotionEvent event) {
                            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                            if (getActivity().getCurrentFocus() != null) {
                                imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                            }
                            return false;
                        }
                    });
                }
            }
        }

        setupInstance();
        setupView();
        initFragment();
    }

    protected View findViewById(int id) {
        return mView.findViewById(id);
    }

    protected abstract int getInflaterLayout();

    protected void setupInstance(){}

    protected void setupView(){}

    protected abstract void initFragment();
}
