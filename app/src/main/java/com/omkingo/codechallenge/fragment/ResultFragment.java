package com.omkingo.codechallenge.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.omkingo.codechallenge.R;
import com.omkingo.codechallenge.adapter.QuestionAdapter;
import com.omkingo.codechallenge.api.ApiService;
import com.omkingo.codechallenge.api.OnRequestObjectListener;
import com.omkingo.codechallenge.model.RequestObject;
import com.omkingo.codechallenge.model.ResponseObject;

import retrofit2.Call;

public class ResultFragment extends BaseFragment{

    public final static String BUNDLE_ACTION = "ACTION";

    public final static int ACTION_BROWSE = 100;
    public final static int ACTION_SEARCH = 101;

    private int actionType;

    private RelativeLayout search_container;
    private ProgressBar progress_bar;
    private ListView lv_result;
    private EditText et_search;
    private RequestObject requestObj;
    private TextWatcher mTextWatcher;
    private TextView.OnEditorActionListener mActionListener;
    private Handler handler;
    private Runnable queryRunnable;

    public static ResultFragment newInstance(int actionType){
        ResultFragment frag = new ResultFragment();

        Bundle args = new Bundle();
        args.putInt(BUNDLE_ACTION, actionType);
        frag.setArguments(args);

        return frag;
    }

    @Override
    protected int getInflaterLayout() {
        return R.layout.fragment_result;
    }

    @Override
    protected void setupInstance() {
        actionType = getArguments().getInt(BUNDLE_ACTION, ACTION_BROWSE);
        requestObj = new RequestObject();
    }

    @Override
    protected void setupView() {
        search_container = (RelativeLayout) findViewById(R.id.search_container);
        progress_bar = (ProgressBar) findViewById(R.id.progress_bar);
        lv_result = (ListView) findViewById(R.id.lv_result);
        et_search = (EditText) findViewById(R.id.et_search);
    }

    @Override
    protected void initFragment() {
        if (actionType == ACTION_SEARCH){
            search_container.setVisibility(View.VISIBLE);

            initRunnableGetUser();
            initListener();

            et_search.addTextChangedListener(mTextWatcher);
            et_search.setOnEditorActionListener(mActionListener);
        }else{
            onDataQuery();
        }

    }

    /**
     * initial runnable variable
     */
    private void initRunnableGetUser() {

        //setup runnable for Check Promo Code
        handler = new Handler();
        queryRunnable = new Runnable() {
            @Override
            public void run() {
                requestObj.setIntitle(et_search.getText().toString());
                onDataQuery();

                //hide keyboard
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                if (getActivity().getCurrentFocus() != null) {
                    imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                }
            }
        };
    }

    /**
     * init listener variable
     */
    private void initListener() {
        //initial text watcher
        mTextWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {handler.removeCallbacks(queryRunnable);}

            @Override
            public void afterTextChanged(Editable s) {
                final int DELAY_TIME = 1000 /* = 1 sec*/;
                //start timer after text changed
                handler.postDelayed(queryRunnable, DELAY_TIME);
            }
        };

        //initial editor action
        mActionListener = new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    //remove the old handle first
                    handler.removeCallbacks(queryRunnable);
                    handler.post(queryRunnable);
                    return true;
                }
                return false;
            }
        };
    }

    /**
     * Query data from server
     */
    private void onDataQuery(){
        progress_bar.setVisibility(View.VISIBLE);
        lv_result.setVisibility(View.GONE);

        //do Browse api
        ApiService apiService = new ApiService(actionType);
        apiService.callApi(requestObj, new OnRequestObjectListener() {
            @Override
            public void onResponse(Call call, ResponseObject data) {
                lv_result.setVisibility(View.VISIBLE);
                progress_bar.setVisibility(View.GONE);

                QuestionAdapter mAdapter = new QuestionAdapter(getContext(), data.getItems());
                lv_result.setAdapter(mAdapter);
            }

            @Override
            public void onFailure(int code, String message, String url) {
                progress_bar.setVisibility(View.GONE);

            }
        });
    }
}
